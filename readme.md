# Javalin Web Service Application

This application interprets HTTP request methods in the forms of `get`, `post`, `put`, and `delete` requests.

Once the HTTP request is received, the application will establish a connection with the database, check request headers for authorization, and then perform SQL operations on the database if allowed.

### Data Models

The object models that can be created, read, updated, and deleted in this application take the following forms:

##### User
```
{
    "userId": (int),
    "username": (string),
    "email": (string)
}
```

##### Post
```
{
    "postId": (int),
    "title": (string),
    "User::userId": (int),
    "postBody": (string)
}
```
### Logging In / Authorization

Before any operations can be performed, an auth token in the form of the key-value pair `Authorization`:`admin-auth-token` must exist in the HTTP headers.

A POST request from `localhost:7000/login` with the correct credentials in the request body will provide the aforementioned token, and is necessary in order to read, write, update, or delete data from the database.

### User Endpoints

`GET` `/users` will retrieve a list of user objects persistent in the database.
`GET` `/users/{userId}` will retrieve the user object identified with the specified userId.
`GET` `/users/{userId}/posts` will retrive all post objects associated with the specified userId.
`POST` `/users` will create a new user object in the database. The HTTP request body should look like:
```
{
    "username": "{username}",
    "email": "{email}"
}
```
The userId will be created for you. You can verify the user was created with `GET` `/users`.
`PUT` `/users/{userId}` will update both the username and password at the specified userId. The HTTP request body should look like:
```
{
    "username": "{updated username}",
    "email": "{updated email}"
}
```
`DELETE` `/users/{userId}` will delete the user object with the specified userId.

### Post Endpoints

`GET` `/posts` will retrieve a list of post objects persistent in the database.
`GET` `/posts/{postId}` will retrieve the post object identified with the specified postId.
`POST` `/posts` will create a new post object in the database. The HTTP request body should look like:
```
{
    "title": "{title}",
    "userId": {User:userId},
    "postBody": "{body}"
}
```
The `userId` must reference a valid User from the database.
`PUT` `/users/{userId}` will update both the username and password at the specified userId. The HTTP request body should look like:
```
{
    "title": "{updated title}",
    "postBody": "{updated body}"
}
```
`DELETE` `/posts/{postId}` will delete the post object with the specified postId.

### Contributions

Thank you to Carolyn and my fellow Revature cohorts!
