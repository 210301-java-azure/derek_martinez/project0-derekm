import controllers.UserDataController;
import io.javalin.http.Context;
import models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import services.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class UserDataControllerTest {

    @InjectMocks
    private UserDataController userDataController;

    @Mock
    private UserService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllUsersHandler(){
        Context context = mock(Context.class);
        User user1 = new User(20,"uername1","email1");
        List<User> users = new ArrayList<>();
        users.add(user1);

        when(service.getAll()).thenReturn(users);
        userDataController.handleGetAllUsersRequest(context);
        verify(context).json(users);

    }

}
