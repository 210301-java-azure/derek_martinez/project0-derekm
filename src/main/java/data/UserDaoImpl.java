package data;

import models.Post;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import dev.derekm.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public List<User> getAllUsers() {

        List<User> users = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()){

            Statement statement =  connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from users");

            while(resultSet.next()){
                int userId = resultSet.getInt("userid");
                String username = resultSet.getString("username");
                String email = resultSet.getString("email");

                User user = new User(userId,username,email);
                users.add(user);
            }

        } catch(SQLException e){
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return users;
    }

    @Override
    public User getUserById(int userId) {

        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement prepareStatement = connection.prepareStatement("select * from users where userid = ?");
            prepareStatement.setInt(1,userId);
            ResultSet resultSet = prepareStatement.executeQuery();

            if(resultSet.next()){
                String username = resultSet.getString("username");
                String email = resultSet.getString("email");
                logger.info("user retrieved from database by id: " + userId);

                return new User(userId,username,email);
            }
        } catch(SQLException e){
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }

    @Override
    public User addNewUser(User user) {

        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement preparedStatement = connection.prepareStatement("insert into users (username, email) values (?, ?)");
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.executeUpdate();
            logger.info("successfully created user");

        } catch (SQLException e){

            logger.error(e.getClass() + " " + e.getMessage());

        }

        return user;
    }



    @Override
    public void deleteUser(int userId){

        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("delete from users where userid = ?");
            preparedStatement.setInt(1, userId);
            preparedStatement.executeUpdate();
            logger.info("user deleted.");
        } catch(SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }

    }

    @Override
    public void updateUser(int userId, User user) {

        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement preparedStatement = connection.prepareStatement("update users set username = ?, email = ? where userid = ?");
            preparedStatement.setString(1,user.getUsername());
            preparedStatement.setString(2,user.getEmail());
            preparedStatement.setInt(3,userId);
            preparedStatement.executeUpdate();
            logger.info("successfully updated user");


        } catch(SQLException e){

            logger.error(e.getClass() + " " + e.getMessage());

        }

    }

    @Override
    public List<Post> getUserPostsByUserId(int userId) {

        List<Post> posts = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement prepareStatement = connection.prepareStatement("select * from posts where userid = ?");
            prepareStatement.setInt(1,userId);
            ResultSet resultSet = prepareStatement.executeQuery();

            int postCounter = 0;

            while(resultSet.next()){



                int postId = resultSet.getInt("postid");
                String title = resultSet.getString("title");
                String postBody = resultSet.getString("postbody");

                Post post = new Post(postId,title,userId,postBody);
                posts.add(post);
                postCounter++;
            }

            logger.info("retrieved " + postCounter + " posts from the database");
            return posts;

        } catch(SQLException e){ logger.error(e.getClass() + " " + e.getMessage()); }

        return posts;
    }


}
