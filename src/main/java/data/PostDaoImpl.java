package data;

import models.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import dev.derekm.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostDaoImpl implements PostDao {

    private Logger logger = LoggerFactory.getLogger(PostDaoImpl.class);

    @Override
    public List<Post> getAllItems() {

        List<Post> posts = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()){

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from posts");

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set

                int postId = resultSet.getInt("postid");
                String title = resultSet.getString("title");
                int userId = resultSet.getInt("userid");
                String postBody = resultSet.getString("postbody");


                Post post = new Post(postId,title,userId,postBody);
                posts.add(post);
            }

        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

        return posts;

    }

    @Override
    public Post getPostById(int postId) {
        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement prepareStatement = connection.prepareStatement("select * from posts where postid = ?");
            prepareStatement.setInt(1,postId);
            ResultSet resultSet = prepareStatement.executeQuery();

            if(resultSet.next()){

                String title = resultSet.getString("title");
                int userId = resultSet.getInt("userId");
                String postBody = resultSet.getString("postBody");
                logger.info("post retrieved from database by id: " + postId);
                return new Post(postId,title,userId,postBody);
            }

        } catch(SQLException e){
            logger.error(e.getClass() + " " + e.getMessage());
        }

        return null;

    }

    @Override
    public Post addNewPost (Post post) {

        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement preparedStatement = connection.prepareStatement("insert into posts (title, userId, postBody) values (?, ?, ?)");
            preparedStatement.setString(1, post.getTitle());
            preparedStatement.setInt(2, post.getUserId());
            preparedStatement.setString(3, post.getPostBody());
            preparedStatement.executeUpdate();
            logger.info("successfully added post");

        } catch (SQLException e){
            logger.error(e.getClass() + " " + e.getMessage());
        }

        return post;
    }

    @Override
    public void updatePost(int postId, Post post){

        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement preparedStatement = connection.prepareStatement("update posts set title = ?, postBody = ? where postid = ?");
            preparedStatement.setString(1,post.getTitle());
            preparedStatement.setString(2,post.getPostBody());
            preparedStatement.setInt(3,postId);
            preparedStatement.executeUpdate();
            logger.info("successfully updated post");

        } catch(SQLException e){
            logger.error(e.getClass() + " " + e.getMessage());
        }

    }


    @Override
    public void deletePost(int postId) {

        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("delete from posts where postid = ?");
            preparedStatement.setInt(1, postId);
            preparedStatement.executeUpdate();
            logger.info("post deleted");

        } catch(SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
    }
}
