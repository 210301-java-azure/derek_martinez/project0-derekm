package services;

import data.PostDao;
import data.PostDaoImpl;
import models.Post;
import java.util.List;

public class PostService {

    private PostDao postDao = new PostDaoImpl();

    public List<Post> getAll(){
        return postDao.getAllItems();
    }

    public Post getById(int postId){
        return postDao.getPostById(postId);
    }

    public Post add(Post post){
        return postDao.addNewPost(post);
    }

    public void update(int postId, Post post){
        postDao.updatePost(postId, post);
    }

    public void delete(int postId){
        postDao.deletePost(postId);
    }

}
