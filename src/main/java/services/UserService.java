package services;

import data.UserDao;
import data.UserDaoImpl;
import models.Post;
import models.User;

import java.util.List;

public class UserService {

    private final UserDao userDao = new UserDaoImpl();

    public List<User> getAll(){
        return userDao.getAllUsers();
    }
    public User getById(int userId){
        return userDao.getUserById(userId);
    }
    public User add(User user){
        return userDao.addNewUser(user);
    }

    public void delete(int userId){
        userDao.deleteUser(userId);
    }

    public void update(int userId, User user) {
        userDao.updateUser(userId,user);
    }

    public List<Post> getPosts(int userId) {
        return userDao.getUserPostsByUserId(userId);
    }
}
