package controllers;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateLogin(Context context){

        String user = context.formParam("username");
        String pass = context.formParam("password");
        logger.info(user + " attempted login");
        if(user != null && user.equals("gooby")){
            if(pass != null && pass.equals("secret-password")){
                logger.info("login attempt successful");
                context.header("Authorization", "admin-auth-token");
                context.status(200);
                return;

            }
        }
        throw new UnauthorizedResponse("login attempt failed, check credentials");
    }

    public void authorizeToken(Context context){
        logger.info("attempting to authorize token");

        String authHeader = context.header("Authorization");

        if(authHeader != null && authHeader.equals("admin-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.warn("not authorized");
            throw new UnauthorizedResponse();
        }
    }
}
