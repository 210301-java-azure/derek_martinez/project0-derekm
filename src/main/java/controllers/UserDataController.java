package controllers;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.UserService;


public class UserDataController {

    private Logger logger = LoggerFactory.getLogger(UserDataController.class);
    private UserService userService = new UserService();

    public void handleGetAllUsersRequest(Context context){
        logger.info("getting all users");
        context.json(userService.getAll());
    }

    public void handleGetUserByIdRequest(Context context){

        String userId = context.pathParam("id");

        if(userId.matches("^\\d+$")){

            int userIdInput = Integer.parseInt(userId);
            User user = userService.getById(userIdInput);

            if(user == null){
                logger.warn("no user found with id: " + userId);
                throw new NotFoundResponse("No user found with provided id:" + userId);
            } else{
                context.json(user);
            }
        } else{
            throw new BadRequestResponse("id input: \"" + userId + "\"cannot be parsed to an int");
        }
    }

    public void handleCreateUserRequest(Context context){
        User user = context.bodyAsClass(User.class);
        logger.info("attempting to add new user: " + user);
        userService.add(user);
        context.status(201);
    }

    public void handleDeleteUserByIdRequest(Context context){

        String userIdString = context.pathParam("id");
        if(userIdString.matches("^\\d+$")){
            int userId = Integer.parseInt(userIdString);
            logger.info("deleting user with id: " + userId);
            userService.delete(userId);
        } else{
            throw new BadRequestResponse("input \""+userIdString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateUserByIdRequest(Context context) {

        String userIdString = context.pathParam("id");
        User user = context.bodyAsClass(User.class);

        if(userIdString.matches("^\\d+$")){
            int userId = Integer.parseInt(userIdString);
            logger.info("updating content of post with postId: " + userId);
            userService.update(userId,user);
        }
    }

    public void handleGetPostsByUserIdRequest(Context context) {
        String userIdString = context.pathParam("id");

        if(userIdString.matches("^\\d+$")){
            int userId = Integer.parseInt(userIdString);
            logger.info("retrieving all posts authored by userid:" + userId);
            context.json(userService.getPosts(userId));
        }
    }
}
