package dev.derekm.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;

    public static Connection getConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(connection == null || connection.isClosed()) {
            // create a connection
            connection = DriverManager.getConnection(System.getenv("connectionString"));
        }
        return connection;
    }


    public static Connection getHardcodedConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(connection == null || connection.isClosed()) {
            String connectionUrl = "jdbc:postgresql://url:5432/postgres";
            String username = "user@server";
            String password = "pass";

            // create a connection
            connection = DriverManager.getConnection(connectionUrl, username, password);
        }
        return connection;
    }
}
